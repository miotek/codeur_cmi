﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace BACK_DIGITS.Models
{
    public class ErrorViewModel
    {
        public HttpStatusCode Code { get; set; }
        public string Error { get; set; }
    }
}