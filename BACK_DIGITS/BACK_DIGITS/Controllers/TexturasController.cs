﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using BACK_DIGITS.Models;
using Microsoft.AspNetCore.Mvc;

namespace BACK_DIGITS.Controllers
{
    public class TexturasController : ApiController
    {
        private DS_Entities db = new DS_Entities();

        // GET: api/Texturas
        public IQueryable<Texturas> GetTexturas()
        {
            return db.Texturas;
        }

        //// GET: api/Texturas/5
        //[ResponseType(typeof(Texturas))]
        //public IHttpActionResult GetTexturas(int id)
        //{
        //    Texturas texturas = db.Texturas.Find(id);
        //    if (texturas == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(texturas);
        //}

        //// PUT: api/Texturas/5
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutTexturas(int id, Texturas texturas)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != texturas.Id)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(texturas).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!TexturasExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        //// POST: api/Texturas
        //[ResponseType(typeof(Texturas))]
        //public IHttpActionResult PostTexturas(Texturas texturas)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.Texturas.Add(texturas);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = texturas.Id }, texturas);
        //}

        // POST: api/Upload
        [ResponseType(typeof(void))]
        [Consumes("multipart/form-data")]
        public async Task<IHttpActionResult> PostUploadTextura()
        {
            if (!Request.Content.IsMimeMultipartContent())
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);

            var provider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(provider);
            foreach (var file in provider.Contents)
            {
                var filename = file.Headers.ContentDisposition.FileName.Trim('\"');
                var buffer = await file.ReadAsByteArrayAsync();
                //Do whatever you want with filename and its binary data.
                //GUARDAR FICHERO FTP
                SaveFileFTP(buffer);
            }

            return Ok();
        }
        // POST: api/Texturas
        [ResponseType(typeof(Texturas))]
        [Consumes("multipart/form-data")]
        public IHttpActionResult PostTexturas(HttpPostedFileBase file)
        {

            if (file.ContentLength > 0)
            {
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(HttpContext.Current.Server.MapPath("~/uploads"), fileName);
                file.SaveAs(path);
                SaveFileFTP(File.ReadAllBytes(path));
            }
            return Ok();// "/uploads/" + file.FileName;
        }

        // POST: api/Upload
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PostUploadTextura()
        //{
        //    if (!Request.Content.IsMimeMultipartContent())
        //        throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);

        //    var provider = new MultipartMemoryStreamProvider();
        //    Request.Content.ReadAsMultipartAsync(provider);
        //    foreach (var file in provider.Contents)
        //    {
        //        var filename = file.Headers.ContentDisposition.FileName.Trim('\"');
        //        var buffer = file.ReadAsByteArrayAsync();
        //        //Do whatever you want with filename and its binary data.
        //        //GUARDAR FICHERO FTP
        //        SaveFileFTP(buffer.Result);
        //    }

        //    return Ok();
        //}

        public void SaveFileFTP(byte[] bytes)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create("sftp://192.168.5.131");
            request.Method = WebRequestMethods.Ftp.UploadFile;

            // This example assumes the FTP site uses anonymous logon.
            request.Credentials = new NetworkCredential("digits", "Miotek2020*");

            // Copy the contents of the file to the request stream.
            byte[] fileContents;
            using (StreamReader sourceStream = new StreamReader(new MemoryStream(bytes), Encoding.UTF8))
            {
                
                fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
            }

            request.ContentLength = fileContents.Length;

            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(fileContents, 0, fileContents.Length);
            }

            using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
            {
                Console.WriteLine($"Upload File Complete, status {response.StatusDescription}");
            }
        }

        //// DELETE: api/Texturas/5
        //[ResponseType(typeof(Texturas))]
        //public IHttpActionResult DeleteTexturas(int id)
        //{
        //    Texturas texturas = db.Texturas.Find(id);
        //    if (texturas == null)
        //    {
        //        return NotFound();
        //    }

        //    db.Texturas.Remove(texturas);
        //    db.SaveChanges();

        //    return Ok(texturas);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TexturasExists(int id)
        {
            return db.Texturas.Count(e => e.Id == id) > 0;
        }
    }
}