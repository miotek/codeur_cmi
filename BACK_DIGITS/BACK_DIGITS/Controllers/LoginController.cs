﻿using BACK_DIGITS.Helper;
using BACK_DIGITS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace BACK_DIGITS.Controllers
{
    public class LoginController : ApiController
    {
        private DS_Entities db = new DS_Entities();
        // POST: api/Login
        [ResponseType(typeof(Usuarios))] 
        public IHttpActionResult PostCheck_Login(LoginViewModel login)
        {
            string code_login = Security.CreateMD5(login.password);
            Usuarios usuarios = db.Usuarios.SingleOrDefault(x => x.Email == login.email && x.Password == code_login);
            if (usuarios == null)
            {
                return NotFound();
            }
            usuarios.Ultimo_Acceso = DateTime.Now;
            db.SaveChanges();
            return Ok(usuarios);
        }
    }
}
